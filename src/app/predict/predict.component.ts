import { Component, OnInit } from '@angular/core';
import { ProjectsService } from './../projects.service';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ClassifyService } from '../classify.service';


@Component({
  selector: 'app-predict',
  templateUrl: './predict.component.html',
  styleUrls: ['./predict.component.css']
})


export class PredictComponent implements OnInit {

  constructor(private projectService:ProjectsService,
    public authService: AuthService, private router:Router,private classifyservice:ClassifyService,) { }
    userID;
    milestone$ : Observable<any>; 
    milestoneID: any; 
    oneMilestone : Observable<any>; 
    category:string;
    Drop;
    DropType;
    PorYYYYWW;
    ProjectID;
    kpi1;
    kpi2;
    kpi3;
    kpi4;
    kpi5;
    result;
    Details$

  ngOnInit() {
    this.authService.getUser().subscribe(
      user =>
      {
        this.userID=user.uid;
        console.log("The userID: ",this.userID)
        this.milestone$= this.projectService.getMilestones(this.userID);
      }
     )
  }

  onSubmit(){ 

    this.projectService.getOnemilestone(this.userID,this.category).subscribe(
      milestone =>
      {
        console.log("this is the milestone.data Drop: ",milestone.data().Drop)
        console.log("this is the milestone.data DropType: ",milestone.data().DropType)
        console.log("this is the milestone.data PorYYYYWW: ",milestone.data().PorYYYYWW)
        console.log("this is the milestone.data ProjectID: ",milestone.data().ProjectID)
        console.log("this is the milestone.data kpi1: ",milestone.data().kpi1)
        console.log("this is the milestone.data kpi2: ",milestone.data().kpi2)
        console.log("this is the milestone.data kpi3: ",milestone.data().kpi3)
        console.log("this is the milestone.data kpi4: ",milestone.data().kpi4)
        console.log("this is the milestone.data kpi5: ",milestone.data().kpi5)

        this.Drop = milestone.data().Drop;
        this.DropType = milestone.data().DropType;
        this.PorYYYYWW = milestone.data().PorYYYYWW;
        this.ProjectID = milestone.data().ProjectID;
        this.kpi1 = milestone.data().kpi1;
        this.kpi2 = milestone.data().kpi2;
        this.kpi3 = milestone.data().kpi3;
        this.kpi4 = milestone.data().kpi4;
        this.kpi5 = milestone.data().kpi5;
        this.classifyservice.doc = this.ProjectID + " " + this.PorYYYYWW + " " + this.Drop + " " + this.DropType + " " + this.kpi1 + " " + this.kpi2 + " " + this.kpi3 + " " + this.kpi4 + " " + this.kpi5;
        console.log("this is the all data: " , this.classifyservice.doc)
        this.router.navigate(['/projectResultComponent/' + this.category]);
        
      }
      
    )
  } 

}

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { ProjectsService } from '../projects.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(public authService: AuthService, private projectService:ProjectsService) { }
  userID
  Details$
  ngOnInit() {
    this.authService.getUser().subscribe(
      user =>
      {
        this.userID=user.uid;
        console.log("The userID: ",this.userID)
        this.Details$= this.projectService.getDetails(this.userID);
      }
     )
  }

}

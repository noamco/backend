import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(private http: HttpClient) { }

  addData(data,table){
    return this.http.post("http://ec2-54-160-95-96.compute-1.amazonaws.com/api/write",{data:data,TableName:table})
  }

  getData(table){
    return this.http.post("http://ec2-54-160-95-96.compute-1.amazonaws.com/api/read",{TableName:table})
  }

  getOneData(no,table){
    return this.http.post("http://ec2-54-160-95-96.compute-1.amazonaws.com/api/get",{no:no,TableName:table})
  }

  deleteData(no,table){
    return this.http.post("http://ec2-54-160-95-96.compute-1.amazonaws.com/api/delete",{no:no,TableName:table})
  }
}

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import {  ActivatedRoute, Router  } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  
  constructor(public auth: AuthService, private router: Router, private route:ActivatedRoute) { 
    this.auth.getLoginErrors().subscribe( error=> {
     this.errorMessege = error;
    });
    this.auth.masageerror ="";
   }
 
  hide = true;
  email: string;
  password: string;
  name: string;
  Emailmassage:string;
  passwordmassage:string; 
  namemassage:string; 

  //hide = true;

  public errorMessege;

  ngOnInit() {
  
  }

  onEmailChange(searchValue: string): void {  
    if(searchValue != null){
      console.log(searchValue)
    this.Emailmassage="";
    this.auth.masageerror = null
  }
}

focusOutEmail(searchValue: string){
if(searchValue == ""){
  this.email = null
}
}

onPassChange(searchValue: string): void {  
  if(searchValue != null){
  this.passwordmassage="";
  this.auth.masageerror = null
}
}

focusOutPass(searchValue: string){
if(searchValue == ""){
  this.password = null
}
}

onNameChange(searchValue: string): void {  
  if(searchValue != null){
  this.namemassage="";
  this.auth.masageerror = null
}
}

focusOutName(searchValue: string){
if(searchValue == ""){
  this.name = null
}
}
  onSubmit(){
    if (this.email == null && this.password == null && this.name == null){
      this.Emailmassage = "Email is required"
      this.passwordmassage = "Password is required"
      this.namemassage = "Full name is required"
    }
    else if(this.email == null && this.password == null ){
      this.Emailmassage = "Email is required"
      this.passwordmassage = "Password is required"
    }
    else if(this.email == null && this.name == null ){
      this.Emailmassage = "Email is required"
      this.namemassage = "Full name is required"
    }
    else if(this.password == null && this.name == null ){
      this.passwordmassage = "Password is required"
      this.namemassage = "Full name is required"
    }
    else if(this.name == null){
      this.passwordmassage = "Full name is required"
    }
    else if(this.email == null){
      this.passwordmassage = "Email is required"
    }
    else if(this.password == null){
      this.passwordmassage = "Password is required"
    }
    else{
    this.auth.signup(this.email, this.password, this.name);
    }
  }


}


// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyA6R4LePq5gEHx3c962cIUCr375tTmK990",
    authDomain: "noamdan-af2db.firebaseapp.com",
    databaseURL: "https://noamdan-af2db.firebaseio.com",
    projectId: "noamdan-af2db",
    storageBucket: "noamdan-af2db.appspot.com",
    messagingSenderId: "721483914746",
    appId: "1:721483914746:web:04626834d69fd9522ed843",
    measurementId: "G-WDC96CKYBH"
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

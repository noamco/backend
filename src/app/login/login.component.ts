import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  //neriya
  hide = true;
  email:string;
  password:string; 
  public errorMessege;
  Emailmassage:string;
  passwordmassage:string; 

  constructor(public auth:AuthService, private route:ActivatedRoute,
    private router:Router) {
      this.auth.getLoginErrors().subscribe( error=> {
        this.errorMessege = error;
       });
       this.auth.masageerror ="";
     }
     
  onSubmit(){
    if (this.email == null && this.password == null){
      this.Emailmassage = "Email is required"
      this.passwordmassage = "Password is required"
    }
    else if(this.email == null){
      this.Emailmassage = "Email is required"
    }
    else if(this.password == null){
      this.passwordmassage = "Password is required"
    }
    else{
    this.auth.login(this.email,this.password);
  }
    
    
  }

  onEmailChange(searchValue: string): void {  
        if(searchValue != null){
          console.log(searchValue)
        this.Emailmassage="";
        this.auth.masageerror = null
      }
  }

  focusOutEmail(searchValue: string){
    if(searchValue == ""){
      this.email = null
    }
  }

  onPassChange(searchValue: string): void {  
      if(searchValue != null){
      this.passwordmassage="";
      this.auth.masageerror = null
    }
  }

  focusOutPass(searchValue: string){
    if(searchValue == ""){
      this.password = null
    }
  }

  ngOnInit() {
  }
}
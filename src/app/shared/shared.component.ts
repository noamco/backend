import { Component, OnInit } from '@angular/core';
import { ProjectsService } from './../projects.service';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shared',
  templateUrl: './shared.component.html',
  styleUrls: ['./shared.component.css']
})
export class SharedComponent implements OnInit {

  milestone$ : Observable<any>; 
  milestone: any; 
  userID;
  constructor(private projectService:ProjectsService,
    public authService: AuthService, private router:Router) { }

  ngOnInit() {
    this.authService.getUser().subscribe(
      user =>
      {
        this.userID=user.uid;
        console.log("The userID: ",this.userID)
        this.milestone$= this.projectService.getShered(this.userID);
      }
     )
  }

  deleteShered(key_id:string)
  {
    console.log("In deleteShered() ")
    console.log("this is the key_id: ",key_id)
    console.log("this is the userID: ",this.userID)
    this.projectService.deleteShered(this.userID,key_id);
  }

}

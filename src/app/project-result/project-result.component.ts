import { Component, OnInit } from '@angular/core';
import { ProjectsService } from './../projects.service';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ClassifyService } from '../classify.service';



@Component({
  selector: 'app-project-result',
  templateUrl: './project-result.component.html',
  styleUrls: ['./project-result.component.css']
})
export class ProjectResultComponent implements OnInit {


  constructor( private projectService:ProjectsService,
    public authService: AuthService, private router:Router, private route:ActivatedRoute, public classifyService:ClassifyService,) { }

   // category:string = "Loading...";
    result:string = "Loading...";
    doc:string;
   
    
    currentDate = new Date();
    userID;
    milestone$ : Observable<any>;
    milestoneID;
    Drop;
    DropType;
    PorYYYYWW;
    ProjectID;
    kpi1;
    kpi2;
    kpi3;
    kpi4;
    kpi5;
    Details$;


  ngOnInit() {
    this.authService.getUser().subscribe(
      user =>
      {
        this.userID=user.uid;
        this.Details$= this.projectService.getDetails(this.userID);
        this.milestoneID=this.route.snapshot.params.category;

      //  this.result = "IsLate" // wait for model
        console.log("The userID: ",this.userID)
        console.log("The milestoneID:" + this.milestoneID)
        this.projectService.getOnemilestone(this.userID,this.milestoneID).subscribe(
        milestone =>
        {
          console.log("this is the milestone.data Drop: ",milestone.data().Drop)
          console.log("this is the milestone.data DropType: ",milestone.data().DropType)
          console.log("this is the milestone.data PorYYYYWW: ",milestone.data().PorYYYYWW)
          console.log("this is the milestone.data ProjectID: ",milestone.data().ProjectID)
          console.log("this is the milestone.data kpi1: ",milestone.data().kpi1)
          console.log("this is the milestone.data kpi2: ",milestone.data().kpi2)
          console.log("this is the milestone.data kpi3: ",milestone.data().kpi3)
          console.log("this is the milestone.data kpi4: ",milestone.data().kpi4)
          console.log("this is the milestone.data kpi5: ",milestone.data().kpi5)

          this.Drop = milestone.data().Drop;
          this.DropType = milestone.data().DropType;
          this.PorYYYYWW = milestone.data().PorYYYYWW;
          this.ProjectID = milestone.data().ProjectID;
          this.kpi1 = milestone.data().kpi1;
          this.kpi2 = milestone.data().kpi2;
          this.kpi3 = milestone.data().kpi3;
          this.kpi4 = milestone.data().kpi4;
          this.kpi5 = milestone.data().kpi5;
         
        }
      );
      }
     )

     this.classifyService.classify().subscribe(
      res => {
        console.log(res);
        console.log(this.classifyService.categories[res])
        this.result = this.classifyService.categories[res]; 
        console.log(this.classifyService.doc)
      }
    )

  }


  saveClassification()
  {
    if(this.result==null){
      alert("You have no result, please ask from the manager to open the Endpoint")
    }
      this.projectService.addClassification(this.userID, this.milestoneID, this.ProjectID, this.PorYYYYWW, this.Drop, this.DropType, this.result, this.currentDate, this.kpi1, this.kpi2, this.kpi3, this.kpi4, this.kpi5 );
      console.log("added!!");
      this.router.navigate(['/projects']);
     
  }


}

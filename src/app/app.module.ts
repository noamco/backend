
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { SignupComponent } from './signup/signup.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { ArticlesComponent } from './articles/articles.component';
import { ClassifyComponent } from './classify/classify.component';
import { WelcomeComponent } from './welcome/welcome.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import { HttpClientModule } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AuthService } from './auth.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FirstpageComponent } from './firstpage/firstpage.component';
import { ProjectsComponent } from './projects/projects.component';
import { AddProjectComponent } from './add-project/add-project.component';
import { PredictComponent } from './predict/predict.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedComponent } from './shared/shared.component';
import { ShareComponent } from './share/share.component';
import { ProjectResultComponent } from './project-result/project-result.component';
import { ProjectShereWithComponent } from './project-shere-with/project-shere-with.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CompletionFormComponent } from './completion-form/completion-form.component';
import {CrudService} from "./crud.service"




const appRoutes: Routes = [
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'projects', component: ProjectsComponent },
  { path: 'addProject', component: AddProjectComponent },
  { path: 'predict', component: PredictComponent },
  { path: 'shared', component: SharedComponent },
  { path: 'firstpage', component: FirstpageComponent },
  { path: 'projectResultComponent/:category', component: ProjectResultComponent },
  { path: 'projectResultComponent', component: ProjectResultComponent },
  { path: 'projectShereWithComponent/:milestone_id/:result/:from', component: ProjectShereWithComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'CompletionForm/:key', component: CompletionFormComponent },
  { path: '',
    redirectTo: 'firstpage',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SignupComponent,
    LoginComponent,
    ArticlesComponent,
    ClassifyComponent,
    WelcomeComponent,
    FirstpageComponent,
    ProjectsComponent,
    AddProjectComponent,
    PredictComponent,
    SharedComponent,
    ShareComponent,
    ProjectResultComponent,
    ProjectShereWithComponent,
    DashboardComponent,
    CompletionFormComponent,
  ],
  imports: [
    
    ReactiveFormsModule,
    MatCheckboxModule,
    MatRadioModule,
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    NoopAnimationsModule,
    MatFormFieldModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule,
    AngularFirestoreModule,
    // AngularFireStorageModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebase),
    RouterModule
    .forRoot(
      appRoutes,
    //   {enableTracing:true}    // this is for debugging only
    )

  ],
  providers: [CrudService,AngularFireAuth, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { User } from './interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Observable<User | null>
  userID
  massegse:string;
  public masageerror;
  logInErrorSubject = new Subject<string>();
  constructor(public db:AngularFirestore, public afAuth:AngularFireAuth,private router:Router, private route:ActivatedRoute ) { this.user = this.afAuth.authState}

  signup(email:string , password:string, name:string){
    this.afAuth
      .auth
      .createUserWithEmailAndPassword(email,password)
      .then(
        (res) => 
        {
          const user1 = {email:email,name:name}
           this.getUser().subscribe(
            user =>
            {
             this.db.collection(`managers/${user.uid}/Details`).add(user1);
             this.addUser(email, name, user.uid )
              console.log("signup successfully")
             
              this.router.navigate(['/welcome'])
            } )  }).catch(error => this.masageerror = error
              )
              this.massegse = "Sign up successfully";
            }

  addUser(email:string , name:string, userID:string){

    const user = {email:email,name:name, userID:userID}
    //this.db.collection('books').add(book)
    this.db.collection(`users`).add(user);
    this.router.navigate(['/welcome'])
  }


  logout(){
    this.afAuth.auth.signOut().then(res => console.log('Yahuuu', res));
    this.router.navigate (['/'])
  }

  login(email:string, password:string){
    this.afAuth.auth.signInWithEmailAndPassword(email,password).then(
      (res) => 
      {
        console.log("Login successfully")
        this.router.navigate(['/welcome'])
      }
    )
    .catch(error => this.masageerror = error
      )
      this.massegse = "Succesful Login";                
    }

    getUser()
    {
      //console.log("user Observable: ",this.user)
      return this.user;
    }

    
  public getLoginErrors():Subject<string>{
    return this.logInErrorSubject;
  }

  getMessage(){
    return this.massegse;
  }

}


 

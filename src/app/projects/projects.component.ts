import { ProjectsService } from './../projects.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { CSVService } from '../CSV.service';
import {CrudService} from "../crud.service"
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  milestone$ : Observable<any>; 
  milestone: any; 
  milestones
  a: any
  saved$ : Observable<any>; 
  saved: any; 
  History$ : Observable<any>; 
  History: any; 
  userID;
  data;
  histories;
 
  constructor( private service :CrudService,public CSVService:CSVService, private projectService:ProjectsService,
    public authService: AuthService, private router:Router) { }

  ngOnInit() {
    console.log(formatDate(new Date(), 'dd-MM-yyyy', 'en'))
    
    this.authService.getUser().subscribe(
      user =>
      {
        this.userID=user.uid;
        console.log("The userID: ",this.userID)
        //this.milestone$= this.projectService.getMilestones(this.userID);

        this.saved$ = this.projectService.getSeved(this.userID);
        
        //this.History$ = this.projectService.getHistory(this.userID)
      }
     )
    console.log("runAgain")
     this.service.getData("Projects").subscribe(res=>{

      this.data=res
      this.milestones=this.data.Items
      console.log(res)
    })
    this.getHistory()
  }

  getHistory(){
    this.service.getData("History").subscribe(res=>{
      this.data=res
      this.histories=this.data.Items
      console.log(this.histories)
      
    })
  }
  
  private selectedLink: string="Current";        
  

  setradio(e: string): void   
  {  
  
    this.selectedLink = e;  
          
  }  
  
    isSelected(name: string): boolean   
  {  
  
        if (!this.selectedLink) { // if no radio button is selected, always return false so every nothing is shown  
            return false;  
  }  
  
        return (this.selectedLink === name); // if current radio button is selected, return true, else return false  
    }  

    deleteSaved(key_id:string)
    {
      console.log("In deleteSaved() ")
      console.log("this is the key_id: ",key_id)
      console.log("this is the userID: ",this.userID)
      this.projectService.deleteSaved(this.userID,key_id);
    }

    deleteHistory(key_id:string)
    {
      console.log("In deleteHistory() ")
      console.log("this is the key_id: ",key_id)
      console.log("this is the userID: ",this.userID)
      //this.projectService.deleteHistory(this.userID,key_id);
      this.service.deleteData(key_id,"History").subscribe(()=>{
        this.getHistory()
      })
    }
  
    finish(key_id:string)
    {
      this.router.navigate(['/CompletionForm/' + key_id]);
    }
  


}

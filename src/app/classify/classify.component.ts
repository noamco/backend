import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClassifyService } from '../classify.service';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {

  constructor(private classifyservice:ClassifyService,
    private router:Router) { }
   text:string;

  
    ngOnInit() {
   
    }
    onSubmit(){
      this.classifyservice.doc = this.text;
      console.log(this.classifyservice.doc)
      this.router.navigate(['/result']);
    }
  
}
